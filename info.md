# Measurement conversions

One cup = 240 ml

One cup of an ingredient is equivalent to ...

Ingredient | Weight in grams
-- | --
Cocoa powder | 120
Icing sugar | 125
Caster sugar | 225
Plain flour | 150
Butter | 225

Oven temperature conversions ...

Fahrenheit | Celcius
-- | --
275 | 140
300 | 150
325 | 170
350 | 180
375 | 190
400 | 200
425 | 220
450 | 230

# Credits

- <https://www.bbcgoodfood.com/recipes/double-chocolate-loaf-cake>
- <https://www.allrecipes.com/recipe/17687/beat-and-bake-orange-cake/>
- <https://www.allrecipes.com/recipe/237023/coconut-coconut-milk-cake/>
- <https://www.which.co.uk/news/2020/06/how-to-convert-ingredient-measurements-in-us-cooking-and-baking-recipes/>
- <https://www.thespruceeats.com/can-sizes-for-recipes-4077057>
