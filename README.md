# Recipes

A compilation of my favourite recipes.
For personal, non-commercial use.

## PDF

Requires Pandoc and TeX Live

```sh
pandoc --variable title="Recipes" --variable documentclass="report" --variable fontfamily="PlayfairDisplay" --variable fontfamilyoptions="osf" --toc --toc-depth=1 recipes/*.md info.md -o recipes.pdf
```
