# Coconut cake

## Ingredients

- 120 ml or 112.5 g white sugar
- 112.5 g softened butter or 120 ml vegetable oil
- 2 separated eggs
- 120 ml unsweetened shredded coconut
- 240 ml or 150 g plain flour
- 1 tablespoon baking powder
- 1/2 teaspoon salt
- 210 ml coconut milk
- pandan leaf extract

## Directions

Preheat oven to 175 degrees.
Grease a cake pan.

Beat sugar, butter, pandan extract, and egg yolks together in a bowl until smooth and creamy.
Add the shredded coconut.

Whisk flour, baking powder, and salt together in a bowl.
Stir the creamed butter mixture, alternating with coconut milk, into the flour
mixture until the batter is just mixed.

Beat the egg whites in a bowl until soft peaks form.
Fold the egg whites into the batter.
Pour the batter into the cake pan.

Bake for about 45 minutes or until a toothpick inserted in the centre of the cake
comes out clean.
