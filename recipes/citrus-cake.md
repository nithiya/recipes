# Citrus cake

## Ingredients

- 480 ml or 300 g plain flour
- 1/2 teaspoon salt
- 3 teaspoons baking powder
- 120 ml or 112.5 g white sugar
- 120 ml milk or yoghurt
- 160 ml citrus fruit juice (2 oranges and 1 lemon)
- 112.5 g softened butter or 120 ml vegetable oil
- 2 beaten eggs
- zest from 2 oranges and 1 lemon

## Directions

Preheat oven to 175 degrees.
Grease and flour a cake pan.

Combine milk or yoghurt, fruit juice, eggs, and zest in a measuring cup.

Sift flour, salt, and baking powder into a large bowl.
Mix in sugar.
Make a well in the centre and pour in the liquid mixture.
Stir until thoroughly combined.
Pour the batter into the cake pan.

Bake for about 35 minutes or until a toothpick inserted into the centre of the
cake comes out clean.
