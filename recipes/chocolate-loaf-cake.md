# Double chocolate loaf cake

## Ingredients

- 175 g softened butter or 187 ml vegetable oil
- 187 ml or 175 g white sugar
- 3 eggs
- 224 ml or 140 g plain flour
- 3 teaspoons baking powder
- 100 ml milk or yoghurt
- 4 tablespoons cocoa powder
- 100 g dark chocolate chips or chunks

## Directions

Preheat oven to 160 degrees.
Grease a loaf tin.

Beat the butter and sugar with a whisk until light and fluffy.
Beat in the eggs, flour, baking powder, milk, and cocoa powder until smooth.
Stir in the chocolate, then scrape the mixture into the tin.

Bake for 45 to 50 minutes until golden and risen, or until a skewer poked in the
centre comes out clean.
